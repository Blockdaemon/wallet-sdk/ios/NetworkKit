// Copyright © Blockdaemon All rights reserved.

import DIKit
import Errors
import Extensions
import Foundation

public protocol NetworkCommunicatorAPI {
    
    /// Performs network requests
    /// - Parameter request: the request object describes the network request to be performed
    func dataTaskPublisher(
        for request: NetworkRequest
    ) -> AnyPublisher<ServerResponse, NetworkError>
}

public protocol NetworkDebugLogger {
    func storeRequest(
        _ request: URLRequest,
        response: URLResponse?,
        error: Error?,
        data: Data?,
        metrics: URLSessionTaskMetrics?,
        session: URLSession?
    )
}

final class NetworkCommunicator: NetworkCommunicatorAPI {
    
    // MARK: - Private properties
    
    private let session: NetworkSession
    @OptionalInject var networkDebugLogger: NetworkDebugLogger?
    private let authenticator: AuthenticatorAPI?
    
    // MARK: - Setup
    
    init(
        session: NetworkSession = resolve(),
        authenticator: AuthenticatorAPI? = nil
    ) {
        self.session = session
        self.authenticator = authenticator
    }
    
    // MARK: - Internal methods
    
    func dataTaskPublisher(
        for request: NetworkRequest
    ) -> AnyPublisher<ServerResponse, NetworkError> {
        guard request.authenticated else {
            return execute(request: request)
        }
        guard let authenticator else {
            fatalError("Authenticator missing")
        }
        let _execute = execute
        return authenticator
            .authenticate { [execute = _execute] token in
                execute(request.adding(authenticationToken: token))
            }
    }
    
    // MARK: - Private methods
    
    private func execute(
        request: NetworkRequest
    ) -> AnyPublisher<ServerResponse, NetworkError> {
        var start: Date = Date()
        let lock = NSLock()
        return session.erasedDataTaskPublisher(
            for: request.peek("🌎 ↑", \.urlRequest.cURLCommand, if: \.isDebugging.request).urlRequest
            )
            .prefix(1)
            .handleEvents(
                receiveSubscription: { _ in
                    lock.withLock { start = Date() }
                },
                receiveOutput: { [session, networkDebugLogger] data, response in
                    if request.isDebugging.request, let httpResponse = response as? HTTPURLResponse {
                        lock.lock()
                        defer { lock.unlock() }
                        response.peek("🌎 ↓ \(httpResponse.statusCode) in \(Date().timeIntervalSince(start))ms", \.url)
                        if let body = try? JSONSerialization.jsonObject(with: data) {
                            response.peek("🧠 ↓ \(body) in \(Date().timeIntervalSince(start))ms", \.url)
                        }
                    }
                    networkDebugLogger?.storeRequest(
                        request.urlRequest,
                        response: response,
                        error: nil,
                        data: data,
                        metrics: nil,
                        session: session as? URLSession
                    )
                },
                receiveCompletion: { [session, networkDebugLogger] completion in
                    guard case .failure(let error) = completion else {
                        return
                    }
                    networkDebugLogger?.storeRequest(
                        request.urlRequest,
                        response: nil,
                        error: error,
                        data: nil,
                        metrics: nil,
                        session: session as? URLSession
                    )
                }
            )
            .mapError { error in
                NetworkError(request: request.urlRequest, type: .urlError(error))
            }
            .flatMap { elements -> AnyPublisher<ServerResponse, NetworkError> in
                request.responseHandler.handle(elements: elements, for: request)
            }
            .eraseToAnyPublisher()
    }
}

protocol NetworkSession {
    func erasedDataTaskPublisher(
        for request: URLRequest
    ) -> AnyPublisher<(data: Data, response: URLResponse), URLError>
}

extension URLSession: NetworkSession {
    func erasedDataTaskPublisher(
        for request: URLRequest
    ) -> AnyPublisher<(data: Data, response: URLResponse), URLError> {
        dataTaskPublisher(for: request)
            .eraseToAnyPublisher()
    }
}
