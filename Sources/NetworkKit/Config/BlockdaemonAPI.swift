// Copyright © Blockdaemon All rights reserved.

import Foundation

public final class BlockdaemonAPI {
    
    // MARK: - Static Properties

    /// The instance variable used to access functions of the `API` class.
    public static let shared = BlockdaemonAPI(credentialProvider: NetworkCredentialProvider())
    
    // MARK: - Properties

    private let credentialProvider: NetworkCredentialProviderAPI

    // MARK: - Initialization

    private init(credentialProvider: NetworkCredentialProviderAPI) {
        self.credentialProvider = credentialProvider
    }
    
    // MARK: - Public Properties

    public var apiUrl: String {
        "https://\(credentialProvider.apiHost)"
    }
    
    public var apiHost: String {
        credentialProvider.apiHost
    }
    
    public var rpId: String {
        credentialProvider.rpId
    }
}
