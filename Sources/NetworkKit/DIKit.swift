// Copyright © Blockdaemon All rights reserved.

import DIKit
import Foundation

extension DependencyContainer {

    // MARK: - NetworkKit Module

    public static var networkKit = module {

        factory { BlockdaemonAPI.shared }

        single { SessionDelegate() as SessionDelegateAPI }

        single { URLSessionConfiguration.defaultConfiguration() }

        single { URLSession.defaultSession() }

        single { Network.Config.defaultConfig }
        
        single { DefaultSessionHandler() as NetworkSessionDelegateAPI }
        
        single { NetworkCredentialProvider() as NetworkCredentialProviderAPI }
        
        single { RequestBuilder() }

        single { BaseRequestBuilder() }

        single { NetworkResponseDecoder() as NetworkResponseDecoderAPI }

        single { NetworkResponseHandler() as NetworkResponseHandlerAPI }

        single { NetworkAdapter.defaultAdapter() as NetworkAdapterAPI }

        single { NetworkCommunicator.defaultCommunicator() as NetworkCommunicatorAPI }

        single { () -> NetworkSession in
            let session: URLSession = DIKit.resolve()
            return session as NetworkSession
        }
    }
}

extension NetworkCommunicator {

    fileprivate static func defaultCommunicator(
        authenticator: AuthenticatorAPI = resolve()
    ) -> NetworkCommunicator {
        NetworkCommunicator(authenticator: authenticator)
    }
}

extension NetworkAdapter {

    fileprivate static func defaultAdapter(
        communicator: NetworkCommunicatorAPI = resolve()
    ) -> NetworkAdapter {
        NetworkAdapter(communicator: communicator)
    }
}

extension URLSession {

    fileprivate static func defaultSession(
        with configuration: URLSessionConfiguration = resolve(),
        sessionDelegate delegate: SessionDelegateAPI = resolve(),
        delegateQueue queue: OperationQueue? = nil
    ) -> URLSession {
        URLSession(configuration: configuration, delegate: delegate, delegateQueue: queue)
    }
}

extension URLSessionConfiguration {

    fileprivate static func defaultConfiguration()
    -> URLSessionConfiguration {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.waitsForConnectivity = true
        sessionConfiguration.httpMaximumConnectionsPerHost = 10
        sessionConfiguration.timeoutIntervalForRequest = 30
        sessionConfiguration.timeoutIntervalForResource = 300

        return sessionConfiguration
    }
}
