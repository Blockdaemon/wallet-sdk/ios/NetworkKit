// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct RawServerResponse: Decodable {
    public let data: String
    public let allHeaderFields: [String: String]?
    
    init(data: String, allHeaderFields: [String : String]? = nil) {
        self.data = data
        self.allHeaderFields = allHeaderFields
    }
}
