// Copyright © Blockdaemon All rights reserved.

import Foundation

public enum Network {

    public struct Config {

        let apiScheme: String?
        let apiHost: String
        let pathComponents: [String]

        static let defaultConfig = Config(
            apiScheme: "https",
            apiHost: BlockdaemonAPI.shared.apiHost,
            pathComponents: []
        )
    }
}

protocol SessionDelegateAPI: URLSessionDelegate {
    var delegate: NetworkSessionDelegateAPI? { get set }
}

class SessionDelegate: NSObject, SessionDelegateAPI {
    weak var delegate: NetworkSessionDelegateAPI?

    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {}

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping AuthChallengeHandler) {
        completionHandler(.performDefaultHandling, nil)
    }

    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {}
}

protocol NetworkSessionDelegateAPI: AnyObject {
    func urlSession(
        _ session: URLSession,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping AuthChallengeHandler
    )
}

class DefaultSessionHandler: NetworkSessionDelegateAPI {
    func urlSession(
        _ session: URLSession,
        didReceive challenge: URLAuthenticationChallenge,
        completionHandler: @escaping AuthChallengeHandler
    ) {
        completionHandler(.performDefaultHandling, nil)
    }
}

public typealias AuthChallengeHandler = (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void
