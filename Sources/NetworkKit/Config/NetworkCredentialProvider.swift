// Copyright © Blockdaemon All rights reserved.

import Foundation

/// A `NetworkCredentialProvider` allows access to credential values stored outside this module.
protocol NetworkCredentialProviderAPI {
    var apiURL: String { get }
    var apiHost: String { get }
    var rpId: String { get }
}

final class NetworkCredentialProvider: NetworkCredentialProviderAPI {

    var apiURL: String {
        "https://\(apiHost)"
    }
    
    var apiHost: String {
        guard let path = Bundle.main.path(forResource: "Info", ofType: "plist") else { fatalError("Expected an Info.plist") }
        guard let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] else { fatalError() }
        guard let apiHost = dict["API_HOST"] as? String else { fatalError("Expected API_HOST") }
        return apiHost
    }
    
    var rpId: String {
        guard let path = Bundle.main.path(forResource: "Info", ofType: "plist") else { fatalError("Expected an Info.plist") }
        guard let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] else { fatalError() }
        guard let rpId = dict["RP_ID"] as? String else { fatalError("Expected RP_ID") }
        return rpId
    }
}
