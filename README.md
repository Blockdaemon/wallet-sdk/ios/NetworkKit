# NetworkKit

This Swift Package is for handling all network requests to any backend service. It is not tightly coupled to any particular service and can be used by iOS, macOS, and likely other Apple platforms. 

For the most part, you will just need to use `NetworkAdapterAPI` and `RequestBuilder`.

Example:

```
    public func foo(
        _ parameter: String
    ) -> AnyPublisher<ResponseObject, SomeErrorType> {
        let payload = Payload(parameter)
        let request = requestBuilder.post(
            path: Path.someURLPath,
            body: try? JSONEncoder().encode(payload)
        )!
        return networkAdapter.perform(request: request)
    }
```

For additional usage examples, see various `APIClient.swift` files in other WalletSDK dependencies. 
