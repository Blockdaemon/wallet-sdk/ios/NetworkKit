// Copyright © Blockdaemon All rights reserved.

import Foundation

public enum HttpHeaderField {
    public static let userAgent = "User-Agent"
    public static let userSession = "user-session"
    public static let authorization = "Authorization"
    public static let accept = "Accept"
    public static let acceptLanguage = "Accept-Language"
    public static let contentLength = "Content-Length"
    public static let contentType = "Content-Type"
    public static let webauthnToken = "wauthn-ceremony-session"
}

public enum HttpHeaderValue {
    public static let json = "application/json"
    public static let formEncoded = "application/x-www-form-urlencoded"
}
