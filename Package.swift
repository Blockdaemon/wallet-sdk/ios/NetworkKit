// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "NetworkKit",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "NetworkKit",
            targets: ["NetworkKit"]),
    ],
    dependencies: [
        .package(
            url: "https://github.com/Liftric/DIKit.git",
            exact: "1.6.1"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Errors",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ToolKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Extensions",
            exact: "1.0.0"
        )
    ],
    targets: [
        .target(
            name: "NetworkKit",
            dependencies: [
                "Errors",
                "ToolKit",
                "Extensions",
                .product(name: "DIKit", package: "DIKit")
            ]),
        .testTarget(
            name: "NetworkKitTests",
            dependencies: ["NetworkKit"]),
    ]
)
